<?php
class Turma
{
    private $CD_Turma;
    private $CD_Professor;
    private $CH_Disciplina;
    private $CH_Semestre;
    private $VF_Ativo;

    public function __construct()
    {}

    public function preencherObjeto($CD_Turma, $CD_Professor, $CH_Disciplina, $CH_Semestre, $VF_Ativo)
    {
        $this->CD_Turma = $CD_Turma;
        $this->CD_Professor = $CD_Professor;
        $this->CH_Disciplina = $CH_Disciplina;
        $this->CH_Semestre = $CH_Semestre;
        $this->VF_Ativo = $VF_Ativo;
    }

    public function __set($key, $value)
    {
        $this->$key = $value;
    }

    public function __get($key)
    {
        return $this->$key;
    }
}
