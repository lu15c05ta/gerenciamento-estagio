
<?php
// teste do banco 
require_once "../bd/conexao.php";
require_once "../model/Empresa.php";
require_once "../model/Pessoa.php";

/* Empresa */
$sql = 'select * from empresa';
$c = Conexao::getInstance()->prepare($sql);
$c->execute();

echo "<pre>";

while ($teste = $c->fetch(PDO::FETCH_ASSOC)) {
    $empresa = new Empresa();
    $empresa->setAllWithArray($teste);
    
    print_r($empresa);
}

/* Pessoa */
$sql = 'select * from pessoa';
$c = Conexao::getInstance()->prepare($sql);
$c->execute();

echo "<pre>";

while ($teste = $c->fetch(PDO::FETCH_ASSOC)) {
    $pessoa = new Pessoa();
    $pessoa->setAllWithArray($teste);
    
    print_r($pessoa);
}
