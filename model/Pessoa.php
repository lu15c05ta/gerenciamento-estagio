<?php
class Pessoa
{
    private $CD_Pessoa;
    private $CH_Nome;
    private $CH_Usuario;
    private $CH_Senha;
    private $VF_Tipo;

    public function __construct()
    {
    }

    public function preencherObjeto($CD_Pessoa, $CH_Nome, $CH_Usuario, $CH_Senha, $VF_Tipo)
    {
        $this->CD_Pessoa  = $CD_Pessoa;
        $this->CH_Nome    = $CH_Nome;
        $this->CH_Usuario = $CH_Usuario;
        $this->CH_Senha   = $CH_Senha;
        $this->VF_Tipo    = $VF_Tipo;
    }

    public function setAllWithArray($arr)
    {
        /*melhorar isso */
        $this->CD_Pessoa = $arr["CD_Pessoa"];
        $this->CH_Nome = $arr["CH_Nome"];
        $this->CH_Usuario = $arr["CH_Usuario"];
        $this->CH_Senha = $arr["CH_Senha"];
        $this->VF_Tipo = $arr["VF_Tipo"];
    }

    public function __set($key, $value)
    {
        $this->$key = $value;
    }

    public function __get($key)
    {
        return $this->$key;
    }
}
