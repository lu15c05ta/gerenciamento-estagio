<?php
class Empresa
{
    private $CD_Empresa;
    private $CH_Fantasia;
    private $CH_CNPJ;
    private $DT_Convenio;
    private $VF_Ativo;

    public function __construct()
    {}

    public function setAll($CD_Empresa, $CH_Fantasia, $CH_CNPJ, $DT_Convenio, $VF_Ativo)
    {
        $this->CD_Empresa = $CD_Empresa;
        $this->CH_Fantasia = $CH_Fantasia;
        $this->CH_CNPJ = $CH_CNPJ;
        $this->DT_Convenio = $DT_Convenio;
        $this->VF_Ativo = $VF_Ativo;
    }

    public function setAllWithArray($arr)
    {        
        /*melhorar isso */
        $this->CD_Empresa = $arr["CD_Empresa"];
        $this->CH_Fantasia = $arr["CH_Fantasia"];
        $this->CH_CNPJ = $arr["CH_CNPJ"];
        $this->DT_Convenio = $arr["DT_Convenio"];
        $this->VF_Ativo = $arr["VF_Ativo"];     
    }

    public function __set($key, $value)
    {
        $this->$key = $value;
    }

    public function __get($key)
    {
        return $this->$key;
    }
}
