-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: gerenciamento_estagio
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empresa` (
  `CD_Empresa` int unsigned NOT NULL AUTO_INCREMENT,
  `CH_CNPJ` varchar(20) DEFAULT NULL,
  `CH_Fantasia` varchar(90) DEFAULT NULL,
  `DT_Convenio` date DEFAULT NULL,
  `VF_Ativo` varchar(1) DEFAULT 'A' COMMENT 'A: Ativo, I: Inativo',
  PRIMARY KEY (`CD_Empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pessoa` (
  `CD_Pessoa` int unsigned NOT NULL AUTO_INCREMENT,
  `CH_Nome` varchar(30) DEFAULT NULL,
  `CH_CPF` varchar(30) DEFAULT NULL,
  `CH_Usuario` varchar(30) DEFAULT NULL COMMENT 'login',
  `CH_Senha` varchar(30) DEFAULT NULL,
  `VF_Tipo` varchar(1) DEFAULT NULL COMMENT 'A: Aluno, P: Professor, C: Coordenador',
  `DOC_Curriculo` longblob,
  `DOC_Relatorio` longblob,
  PRIMARY KEY (`CD_Pessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,'Luis','05311374306','luiscosta','123456','A',NULL,NULL),(2,'Professor','12345678910','professorusuario','111111','P',NULL,NULL),(3,'Hudson','11111212','coordenador','1234','C',NULL,NULL);
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turma`
--

DROP TABLE IF EXISTS `turma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `turma` (
  `CD_Turma` int unsigned NOT NULL AUTO_INCREMENT,
  `CD_Professor` int unsigned NOT NULL,
  `CH_Disciplina` varchar(30) DEFAULT NULL,
  `CH_Semestre` varchar(30) DEFAULT NULL,
  `VF_Ativo` varchar(1) DEFAULT 'A' COMMENT 'A: Ativo, I: Inativo',
  PRIMARY KEY (`CD_Turma`),
  KEY `FK_CD_Pessoa` (`CD_Professor`),
  CONSTRAINT `FK_CD_Pessoa` FOREIGN KEY (`CD_Professor`) REFERENCES `pessoa` (`CD_Pessoa`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turma`
--

LOCK TABLES `turma` WRITE;
/*!40000 ALTER TABLE `turma` DISABLE KEYS */;
/*!40000 ALTER TABLE `turma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turma_aluno`
--

DROP TABLE IF EXISTS `turma_aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `turma_aluno` (
  `CD_Turma_Aluno` int unsigned NOT NULL AUTO_INCREMENT,
  `CD_Aluno` int unsigned DEFAULT NULL,
  `CD_Turma` int unsigned DEFAULT NULL,
  PRIMARY KEY (`CD_Turma_Aluno`),
  KEY `FK_CD_Aluno` (`CD_Aluno`),
  KEY `FK_CD_Turma` (`CD_Turma`),
  CONSTRAINT `FK_CD_Aluno` FOREIGN KEY (`CD_Aluno`) REFERENCES `pessoa` (`CD_Pessoa`) ON UPDATE CASCADE,
  CONSTRAINT `FK_CD_Turma` FOREIGN KEY (`CD_Turma`) REFERENCES `turma` (`CD_Turma`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turma_aluno`
--

LOCK TABLES `turma_aluno` WRITE;
/*!40000 ALTER TABLE `turma_aluno` DISABLE KEYS */;
/*!40000 ALTER TABLE `turma_aluno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vaga`
--

DROP TABLE IF EXISTS `vaga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vaga` (
  `CD_Vaga` int unsigned NOT NULL AUTO_INCREMENT,
  `CD_Aluno` int unsigned DEFAULT NULL,
  `CD_Estagio` int unsigned DEFAULT NULL,
  `DT_Inicio` date DEFAULT NULL,
  `DT_Fim` date DEFAULT NULL,
  `VF_Ativo` varchar(1) DEFAULT 'A' COMMENT 'A: Ativo, I: Inativo',
  `CH_Relatorio` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`CD_Vaga`),
  KEY `FK_CD_Pessoa_vaga` (`CD_Aluno`),
  KEY `FK_CD_Empresa` (`CD_Estagio`),
  CONSTRAINT `FK_CD_Empresa` FOREIGN KEY (`CD_Estagio`) REFERENCES `empresa` (`CD_Empresa`) ON UPDATE CASCADE,
  CONSTRAINT `FK_CD_Pessoa_vaga` FOREIGN KEY (`CD_Aluno`) REFERENCES `pessoa` (`CD_Pessoa`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vaga`
--

LOCK TABLES `vaga` WRITE;
/*!40000 ALTER TABLE `vaga` DISABLE KEYS */;
/*!40000 ALTER TABLE `vaga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'gerenciamento_estagio'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-02 20:02:38
